const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const multer = require('multer')
const path = require('path')
const fileRoutes = require('./routes/files')
const app = express()

// Middleware
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(multer().single('file'))
app.use(cors())

// Routes
app.use('/api-documents/files', fileRoutes)
app.use('/api-documents/upload', (req,res) => res.sendFile(path.join(__dirname + '/index.html')))
app.get('/api-documents/*', (req, res) => res.status(404).set('text/html').send('<h1>Forbidden</h1>'))

module.exports = app
