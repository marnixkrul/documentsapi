const sqlite3 = require('sqlite3').verbose()
const fs = require('fs')
const Config = require('./config')
const config = new Config()

if (!fs.existsSync(config.baseDirectory)){
    fs.mkdirSync(config.baseDirectory)
}
const db = new sqlite3.Database('/data/documentsAPI/volume/index.db')
db.serialize(()=>{
    db.run("CREATE TABLE if NOT EXISTS files (filename TEXT, mimetype TEXT, hash TEXT, uploadedBy TEXT)")
})
module.exports = db