# Documents API
*A simple nodeJS document storage API* \
Based on a key:value store

## default config
* Port: 3000
* Volume basedirectory: '/data/documentsAPI/volume'
   * Can be edited in config.js
* Database basedirectory : '/data/documentsAPI/database/index.db
Uploads will be placed in this volume

## Install
1. Clone project
1. `cd documentsapi`
1. `npm install`
1. `npm start`


## Endpoints
Route: `/files`
 * **POST:** Upload a file

Route: `/upload`
 * **GET:** Created simple upload form

Route: `/files/:file`
 * **GET:** Show/download file
 * **DELETE:** Delete a file

## TODO
* Should delete also delete folders if they are empty? YES.
* Remove upload index.html
* Create config page
   * S3 Bucket sync
   * SQL Queries, columns
   * Salt
