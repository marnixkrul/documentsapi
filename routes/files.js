const express = require('express')
const router = express.Router()
const fs = require('fs')
const Config = require('../config')
const config = new Config
const crypto = require('crypto')
const db = require('../dbconnection')

const getPath = (file) => {
    const f = file.toUpperCase()
    const vol = f.substring(0,1)
    const f1 = f.substring(0,2)
    const f2 = f.substring(0,4)
    const p = '/' + vol + '/' + f1 + '/' + f2
    return p
}

router.route('/')
    .get((req,res) => {
        const  { eID } = req.query
        if (eID){
            db.all("SELECT * FROM files WHERE uploadedBy = ?", eID, (err,rows) => {
                const array = []
                const promise = rows.map((el) => {
                    return new Promise((resolve) => {
                        const {filename, mimetype, uploadedBy} = el
                        const data = {
                            filename,
                            mimetype,
                            uploadedBy
                        }
                        array.push(data)
                        resolve(data)
                      })
                    })
                Promise.all(promise).then((result) => {
                    res.status(200).json(result);
                })
            })
        }
    })
    .post((req, res) => {
        const { originalname: file, buffer: data, mimetype} = req.file
        const meta = JSON.parse(req.body.meta)
        const hash = crypto.createHash('md5').update(file + data + config.salt).digest('hex')
        const info = {
            name: file,
            mimetype,
            hash,
            meta
        }
        const path = getPath(hash)
        
        // if file exists, send back file
        if (fs.existsSync(config.baseDirectory + path + '/' + hash)){
            res.status(302).json({"message":'file found'})
        } else {
            // conflict, check if filename exists in database
            db.all("SELECT * FROM files WHERE filename= ? AND uploadedBy = ?", [info.name, meta.employeeID], (err, row) =>{
                if (row[0]){
                    res.status(409).json({"message": "Conflict, different file with same name found"})
                }
                else{
                    // create folder if it doesn't exist
                    if (!fs.existsSync(config.baseDirectory + path)){
                        fs.mkdirSync(config.baseDirectory + path,{recursive:true}, (err) => {
                            if (err) throw err;
                        })
                    }
                    // create file and write data
                    const wstream = fs.createWriteStream(config.baseDirectory + path + '/' + hash)
                    wstream.write(data)
                    const statement = db.prepare("INSERT INTO files VALUES(?, ?, ?, ?)")
                    statement.run(info.name, info.mimetype, info.hash, info.meta.employeeID)
                    wstream.end()
                    res.status(201).json({"message": "Succesfully created "+ file})
                }
            })
            
        }
    })

router.route('/:file')
.get((req,res) => {
        const { file } = req.params
        const  { eID } = req.query
        db.all("SELECT * FROM files WHERE filename= ? AND uploadedBy = ?",  [file, eID] , (err,row)=>{
            if (err || !row[0]) res.status(404).json({"message": "Could not find file in database", err})
            const info = row[0]
            const path = getPath(info.hash)
            if (!fs.existsSync(config.baseDirectory + path + '/' + info.hash)){
                res.status(404).json({"message":'File not found'})
            } else {
                fs.readFile(config.baseDirectory + path + '/' + info.hash, (err,data) => {
                    res.writeHead(200,{
                        'Content-Type': info.mimetype
                    })
                    res.end(data)
                })
            }
        })
    })
    .delete((req,res) => {
        const { file } = req.params
        const { eID } = req.query
        db.all("SELECT * FROM files WHERE filename=? AND uploadedBy = ? ", [file, eID], (err,row) =>{
            const info = row[0]
            const path = getPath(info.hash)
            fs.unlinkSync(config.baseDirectory + path + '/' + info.hash)
            db.run("DELETE FROM files WHERE filename=(?)", file, (err)=>{
                if (err) console.log(err)
                res.status(200).json({"message": "Successfully deleted " + file})
            })
        })
    })

module.exports = router
